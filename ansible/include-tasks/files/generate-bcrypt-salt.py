import string
import random
from passlib.hash import bcrypt

str_rand = random.SystemRandom().choice(string.ascii_lowercase + string.ascii_uppercase + string.digits)
print(bcrypt.hash(''.join(str_rand for _ in range(64)))[7:29])
