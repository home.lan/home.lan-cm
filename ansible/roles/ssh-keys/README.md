# Purpose
Copies the SSH keys used throughout `home.lan`.

## Generate new keys
```sh
cd {{ ROLE_PATH }}/files

# -t: Specifies the type of key to create.
# -b: Specifies the number of bits in the key to create.
# -N: Provides the new passphrase.
# -C: Provides a new comment.
# -f: Specifies the filename of the key file.
ssh-keygen -t rsa -b 4096 -N "" -C "{{ KEY_NAME }}" -f ./{{ KEY_NAME }}

# Manually vault the private key
#       On Windows:
#           In subsystem Ubuntu:
#               mkdir ~/home.lan
#               chmod 700 ~/home.lan
#               cd ~/home.lan
#               cp /mnt/c/Users/Wolfmah/Documents/projects/gitlab/home.lan/home.lan-cm/ansible/ansible.cfg ~/home.lan/
#               cp /mnt/c/Users/Wolfmah/Documents/projects/gitlab/home.lan/home.lan-cm/ansible/.vault-pass ~/home.lan/
#
cd {{ PROJECT_ANSIBLE_ROOT }}
ansible-vault encrypt ./roles/ssh-keys/files/{{ KEY_NAME }}

# Manually combine the public key into the appropriate authorized_keys

# Add the relevant tasks to tasks/main.yml
```
