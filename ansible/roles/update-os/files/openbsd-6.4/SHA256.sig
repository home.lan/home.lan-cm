untrusted comment: verify with openbsd-64-fw.pub
RWRoBbjnosJ/3ySvYcT7To933Je5O0b8cAp440lRo6AHRn+Yq9I4ncFxHiG5sfbZXnya3svKgYVbOdyS5Wqt0S57d6IPImz+lww=
SHA256 (acx-firmware-1.4p5.tgz) = i9EOwxxNpUcofUG9mhBsHIfG6dqMoUDFhE0p0THpaP8=
SHA256 (athn-firmware-1.1p4.tgz) = V/hElcBF57mlhBYvtduCLqHuhF7XLBb4V9PlwOYC/kg=
SHA256 (bwfm-firmware-20171125.tgz) = sdl4ulM0qza0Y8vWM6rtwE/NAp7o9jMx9Ql0nQyOEMc=
SHA256 (bwi-firmware-1.4p4.tgz) = cQPcBkiHtfAzbOW8LaEzj9zqMcy7twm1tXrb2LX6IDs=
SHA256 (intel-firmware-20180807p0v0.tgz) = rXwDEwW/BFMi1zM7m9WhgGOf69RvpcD4jtZWWIBjk+Y=
SHA256 (ipw-firmware-1.3p2.tgz) = 4HsMo3PA2THJY52Oimwtqa79A7b5r0xH6wcrORMjcFw=
SHA256 (iwi-firmware-3.1p2.tgz) = de/q+7+H7R8ghFIkZ20QLv2xKqRjhwc+i3BulWsZBCo=
SHA256 (iwm-firmware-0.20170105.tgz) = C+XSo0bSSWq9J44323lEGHLWmPmyu/IbrFOQ3X+7VLc=
SHA256 (iwn-firmware-5.11p1.tgz) = db7biFE3tfDna2Z1U4WrkJNr8yCUZdFpA8WRWwxZl3U=
SHA256 (malo-firmware-1.4p4.tgz) = m7I5akSrwY04Pp9SqR1yWtLkjoubsJAtu9gyIZeZJ28=
SHA256 (otus-firmware-1.0p1.tgz) = xIDpyA2Q/M8pYTMzr8blpqie+1x4lHpxwYZzQBJEK2o=
SHA256 (pgt-firmware-1.2p4.tgz) = 73Jd1Bj+dlr8RxhcfpOWRYHVo1NGSWGlZgyBquoo2nM=
SHA256 (radeondrm-firmware-20170119.tgz) = gt+r280C7wkF9LWUjirDyic5gg4dBWZ9U+y+ctRc2po=
SHA256 (rsu-firmware-1.2p1.tgz) = tdwP7tf0IqNcpd8s+/dsuikEfMRh8gBorUnBZUBVIZk=
SHA256 (rtwn-firmware-20180103.tgz) = JxTn+mtHwrcQoOnv5zvJzdxGtzPOc1bKQxFTC1mnWRs=
SHA256 (uath-firmware-2.0p1.tgz) = wtPAZQ3uQ5Da3/lnQsmfQH5/sLIWBtCafOcrqKV1chE=
SHA256 (upgt-firmware-1.1p4.tgz) = lf5h2fuPLNUDznPeIz5sOkavyC9Hm8fwsBrh6VD9Wp0=
SHA256 (urtwn-firmware-20180103.tgz) = R0vylvHNWWwfSNDFMDfh8P0E8hGSGHKZzhhA7MXW3xk=
SHA256 (uvideo-firmware-1.2p2.tgz) = j7P3luzktxudUtl9BHlZWGNpshvTS2NASkvIXpl+lTg=
SHA256 (vmm-firmware-1.11.0p0.tgz) = aCYU44+FgKy2nbBYANojL+jZaZ8yPWwK18AuFxBvglg=
SHA256 (wpi-firmware-3.2p1.tgz) = SmUSDnmEUQGfbTuK0Y0lo7xjtERSwxnpax+v3aJgyNQ=
