---
- name: Install/Update nut
  become: true
  openbsd_pkg:
    name: nut
    state: latest

- import_tasks: "{{ playbook_dir }}/include-tasks/fetch-bcrypt-salt.yml"

# My UPS is plugged in `uhidev#`, and that's a virtual device. All USB virtual
# device seem to be plugged in the physical device `/dev/usb#`. So, just
# changing the ownership of that device for NUT user would render all other USB
# device useless. Instead, we create a new group, add root and nut user to that
# group, and change the USB device ownership to that new gorup.
- name: Change ownership of UPS USB device
  become: true
  block:
    - name: Add group usb
      group:
        name: usb
        state: present
        system: true

    - name: Add root user to group 'usb'
      user:
        name: "{{ root_user }}"
        groups: usb
        append: true

    - name: Add nut user to group 'usb'
      user:
        name: "{{ nut_user }}"
        groups: usb
        append: true

    - name: Change group of the USB device
      file:
        path: "{{ nut.ups.device }}"
        owner: "{{ root_user }}"
        group: usb

- name: Configure upsmon user/folders
  become: true
  block:
    - name: Create a group for upsmon
      group:
        name: "{{ nut.monitor.group }}"
        state: present

    - name: Create a user for upsmon
      user:
        name: "{{ nut.monitor.user }}"
        group: "{{ nut.monitor.group }}"
        groups: "{{ nut.monitor.groups }}"
        password: "{{ nut.monitor.password | password_hash('blowfish', bcrypt_salt_generated.stdout | default(bcrypt_salt_read) | trim) }}"
        state: present
        shell: /sbin/nologin
        create_home: false

    - name: Create /usr/local/ups
      file:
        path: /usr/local/ups
        mode: 0700
        owner: "{{ nut.monitor.user }}"
        group: "{{ root_group }}"
        state: directory

    - name: Copy upssched-cmd.sh
      template:
        src: upssched-cmd.sh.j2
        dest: /usr/local/ups/upssched-cmd.sh
        mode: 0700
        owner: "{{ nut.monitor.user }}"
        group: "{{ root_group }}"

- name: Copy config files
  become: true
  block:
    - name: Copy nut.conf
      template:
        src: nut.conf.j2
        dest: /etc/nut/nut.conf
        mode: 0600
        owner: "{{ nut_user }}"
        group: "{{ root_group }}"
      notify: Restart nut

    - name: Copy ups.conf
      template:
        src: router/ups.conf.j2
        dest: /etc/nut/ups.conf
        mode: 0600
        owner: "{{ nut_user }}"
        group: "{{ root_group }}"
      notify: Restart nut

    - name: Copy upsd.conf
      template:
        src: router/upsd.conf.j2
        dest: /etc/nut/upsd.conf
        mode: 0600
        owner: "{{ nut_user }}"
        group: "{{ root_group }}"
      notify: Restart nut

    - name: Copy upsd.users
      template:
        src: router/upsd.users.j2
        dest: /etc/nut/upsd.users
        mode: 0600
        owner: "{{ nut_user }}"
        group: "{{ root_group }}"
      notify: Restart nut 

    - name: Copy upsmon.conf
      template:
        src: router/upsmon.conf.j2
        dest: /etc/nut/upsmon.conf
        mode: 0600
        owner: "{{ nut_user }}"
        group: "{{ root_group }}"
      notify: Restart nut

    - name: Copy upssched.conf
      template:
        src: upssched.conf.j2
        dest: /etc/nut/upssched.conf
        mode: 0644
        owner: "{{ nut_user }}"
        group: "{{ root_group }}"

    - name: Copy upsset.conf
      template:
        src: router/upsset.conf.j2
        dest: /etc/nut/upsset.conf
        mode: 0644
        owner: "{{ nut_user }}"
        group: "{{ root_group }}"

- name: NUT services
  become: true
  block:
    - name: Enable nut server
      service:
        name: upsd
        enabled: true
        state: started

    - name: Enable nut monitor
      service:
        name: upsmon
        enabled: true
        state: started

# NUT project, by default, uses `/var/state/ups`. But, by digging through the
# OpenBSD sources patches, OpenBSD is using `/var/db/nut`.
- name: Make sure the state path is properly configured
  file:
    path: /var/db/nut
    mode: 0700
    owner: "{{ nut_user }}"
    group: "{{ root_group }}"
    state: directory
