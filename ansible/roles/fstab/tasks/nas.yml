---
- name: Install parted
  become: true
  apt:
    name: parted
    state: latest
    update_cache: true

- name: Install ACL
  become: true
  apt:
    name: acl
    state: latest
    update_cache: true

- name: Determine which /dev device is mounted as root (/)
  shell:
    executable: /bin/bash
    cmd: |
      (IFS='
      '
      for D in `df -h`; do
          if [ "/" == "`echo "$D" | tr -s ' ' | cut -d ' ' -f 6`" ]; then
              echo "`echo "$D" | tr -s ' ' | cut -d ' ' -f 1 | cut -d '/' -f 3`"
          fi
      done
      )
  register: shell_device_root
  changed_when: false
  failed_when: false

- name: Append ACL to root filesystem's options
  when: (shell_device_root.stdout_lines is defined) and (shell_device_root.stdout_lines[0] is defined)
  become: true
  lineinfile:
    dest: /etc/fstab
    regexp: "^UUID={{ ansible_device_links.uuids[shell_device_root.stdout_lines[0]][0] }}"
    line: "UUID={{ ansible_device_links.uuids[shell_device_root.stdout_lines[0]][0] }}    /    ext4    acl,errors=remount-ro    0    1"
    state: present
    create: true
    mode: 0644
    owner: "{{ root_user }}"
    group: "{{ root_group }}"
  register: modified_root_mount

- name: Append custom drives to existing fstab
  become: true
  blockinfile:
    path: /etc/fstab
    block: "{{ lookup('template', 'fstab.j2') }}"
    insertafter: EOF
    state: present
    mode: 0644
    owner: "{{ root_user }}"
    group: "{{ root_group }}"
  register: add_to_fstab

- name: Reboot after copying fstab
  when: ((modified_root_mount.changed is defined) and (modified_root_mount.changed)) or ((add_to_fstab.changed is defined) and (add_to_fstab.changed))
  become: true
  reboot:
