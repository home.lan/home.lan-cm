# Purpose
As of now, only useful for NAS. Specify the connected hard drives.

Some system may need `ACL` active on their filesystem for some operation to work. Most notably, cloning a git repo using a different user than the one used by Ansible.

## OS
- Debian 9.5
