set limit table-entries 2000000
set optimization normal
set limit states 791000
set limit src-nodes 791000

#System aliases

loopback = "{ lo0 }"
WAN = "{ igb3 }"
LAN = "{ igb1 }"
WIFI = "{ igb2 }"
OpenVPN = "{ openvpn }"

#SSH Lockout Table
table <sshlockout> persist
table <webConfiguratorlockout> persist
#Snort tables
table <snort2c>
table <virusprot>
table <bogons> persist file "/etc/bogons"
table <vpn_networks> { 10.0.0.0/24 }
table <negate_networks> { 10.0.0.0/24 }

# User Aliases
table <laptops> {   192.168.2.56  192.168.2.60 }
laptops = "<laptops>"
table <nas_server> {   192.168.1.10 }
nas_server = "<nas_server>"
table <nvidia_shield_chromecast> {   192.168.2.52  192.168.2.53  192.168.2.54  192.168.2.55  192.168.2.57  192.168.2.58  192.168.2.59 }
nvidia_shield_chromecast = "<nvidia_shield_chromecast>"
table <plex_access> {   192.168.2.50  192.168.2.52  192.168.2.53  192.168.2.54  192.168.2.55  192.168.2.57  192.168.2.58  192.168.2.59 }
plex_access = "<plex_access>"
table <printers> {   192.168.1.190 }
printers = "<printers>"
table <web_server> {   192.168.1.15 }
web_server = "<web_server>"
table <wireless_access_points> {   192.168.2.2 }
wireless_access_points = "<wireless_access_points>"

# Gateways
GWWAN_DHCP = " route-to ( igb3 107.179.246.129 ) "


set loginterface igb1

set skip on pfsync0

scrub on $WAN all    fragment reassemble
scrub on $LAN all    fragment reassemble
scrub on $WIFI all    fragment reassemble


no nat proto carp
no rdr proto carp
nat-anchor "natearly/*"
nat-anchor "natrules/*"


# Outbound NAT rules (automatic)

# Subnets to NAT
tonatsubnets    = "{ 127.0.0.0/8 192.168.1.0/24 192.168.2.0/24 10.0.0.0/24 }"
nat on $WAN  from $tonatsubnets to any port 500 -> 107.179.246.146/32  static-port
nat on $WAN  from $tonatsubnets to any -> 107.179.246.146/32 port 1024:65535

# Load balancing anchor
rdr-anchor "relayd/*"
# TFTP proxy
rdr-anchor "tftp-proxy/*"
# NAT Inbound Redirects
rdr on igb3 proto tcp from any to 107.179.246.146 port 443 -> $web_server
# Reflection redirects
rdr on { igb1 igb2 openvpn } proto tcp from any to 107.179.246.146 port 443 tag PFREFLECT -> 127.0.0.1 port 19000

rdr on igb3 proto tcp from any to 107.179.246.146 port 4443 -> $web_server
# Reflection redirects
rdr on { igb1 igb2 openvpn } proto tcp from any to 107.179.246.146 port 4443 tag PFREFLECT -> 127.0.0.1 port 19001

rdr on igb3 proto tcp from any to 107.179.246.146 port 80 -> $web_server
# Reflection redirects
rdr on { igb1 igb2 openvpn } proto tcp from any to 107.179.246.146 port 80 tag PFREFLECT -> 127.0.0.1 port 19002

rdr on igb3 proto { tcp udp } from any to 107.179.246.146 port 32400 -> 192.168.1.10
# Reflection redirects
rdr on { igb1 igb2 openvpn } proto { tcp udp } from any to 107.179.246.146 port 32400 tag PFREFLECT -> 127.0.0.1 port 19003

rdr on igb3 proto tcp from any to 107.179.246.146 port 15632 -> $web_server
# Reflection redirects
rdr on { igb1 igb2 openvpn } proto tcp from any to 107.179.246.146 port 15632 tag PFREFLECT -> 127.0.0.1 port 19004

# UPnPd rdr anchor
rdr-anchor "miniupnpd"

anchor "relayd/*"
anchor "openvpn/*"
anchor "ipsec/*"
# Allow IPv6 on loopback
pass in  quick on $loopback inet6 all tracker 1000105481 label "pass IPv6 loopback"
pass out  quick on $loopback inet6 all tracker 1000105482 label "pass IPv6 loopback"
# Block all IPv6
block in log quick inet6 all tracker 1000105483 label "Block all IPv6"
block out log quick inet6 all tracker 1000105484 label "Block all IPv6"
# block IPv4 link-local. Per RFC 3927, link local "MUST NOT" be forwarded by a routing device,
# and clients "MUST NOT" send such packets to a router. FreeBSD won't route 169.254./16, but
# route-to can override that, causing problems such as in redmine #2073
block in log quick from 169.254.0.0/16 to any tracker 1000105581 label "Block IPv4 link-local"
block in log quick from any to 169.254.0.0/16 tracker 1000105582 label "Block IPv4 link-local"
#---------------------------------------------------------------------------
# default deny rules
#---------------------------------------------------------------------------
block in log inet all tracker 1000105583 label "Default deny rule IPv4"
block out log inet all tracker 1000105584 label "Default deny rule IPv4"
block in log inet6 all tracker 1000105585 label "Default deny rule IPv6"
block out log inet6 all tracker 1000105586 label "Default deny rule IPv6"
# We use the mighty pf, we cannot be fooled.
block log quick inet proto { tcp, udp } from any port = 0 to any tracker 1000105587 label "Block traffic from port 0"
block log quick inet proto { tcp, udp } from any to any port = 0 tracker 1000105588 label "Block traffic to port 0"

# Snort package
block log quick from <snort2c> to any tracker 1000105589 label "Block snort2c hosts"
block log quick from any to <snort2c> tracker 1000105590 label "Block snort2c hosts"

# SSH lockout
block in log quick proto tcp from <sshlockout> to (self) port 13267 tracker 1000105781 label "sshlockout"

# webConfigurator lockout
block in log quick proto tcp from <webConfiguratorlockout> to (self) port 444 tracker 1000105831 label "webConfiguratorlockout"
block in log quick from <virusprot> to any tracker 1000000400 label "virusprot overload table"
# block bogon networks (IPv4)
# http://www.cymru.com/Documents/bogon-bn-nonagg.txt
block in log quick on $WAN from <bogons> to any tracker 11000 label "block bogon IPv4 networks from WAN"
antispoof log for $WAN tracker 1000107050
# block anything from private networks on interfaces with the option set
block in log quick on $WAN from 10.0.0.0/8 to any tracker 12000 label "Block private networks from WAN block 10/8"
block in log quick on $WAN from 127.0.0.0/8 to any tracker 12000 label "Block private networks from WAN block 127/8"
block in log quick on $WAN from 172.16.0.0/12 to any tracker 12000 label "Block private networks from WAN block 172.16/12"
block in log quick on $WAN from 192.168.0.0/16 to any tracker 12000 label "Block private networks from WAN block 192.168/16"
block in log quick on $WAN from fc00::/7 to any tracker 12000 label "Block ULA networks from WAN block fc00::/7"
# allow our DHCP client out to the WAN
pass in  on $WAN proto udp from any port = 67 to any port = 68 tracker 1000107071 label "allow dhcp client out WAN"
pass out  on $WAN proto udp from any port = 68 to any port = 67 tracker 1000107072 label "allow dhcp client out WAN"
# Not installing DHCP server firewall rules for WAN which is configured for DHCP.
antispoof log for $LAN tracker 1000108100
# allow access to DHCP server on LAN
pass in  quick on $LAN proto udp from any port = 68 to 255.255.255.255 port = 67 tracker 1000108121 label "allow access to DHCP server"
pass in  quick on $LAN proto udp from any port = 68 to 192.168.1.1 port = 67 tracker 1000108122 label "allow access to DHCP server"
pass out  quick on $LAN proto udp from 192.168.1.1 port = 67 to any port = 68 tracker 1000108123 label "allow access to DHCP server"
# allow access to DHCPv6 server on LAN
# We need inet6 icmp for stateless autoconfig and dhcpv6
pass  quick on $LAN inet6 proto udp from fe80::/10 to fe80::/10 port = 546 tracker 1000108131 label "allow access to DHCPv6 server"
pass  quick on $LAN inet6 proto udp from fe80::/10 to ff02::/16 port = 546 tracker 1000108132 label "allow access to DHCPv6 server"
pass  quick on $LAN inet6 proto udp from fe80::/10 to ff02::/16 port = 547 tracker 1000108133 label "allow access to DHCPv6 server"
pass  quick on $LAN inet6 proto udp from ff02::/16 to fe80::/10 port = 547 tracker 1000108134 label "allow access to DHCPv6 server"
antispoof log for $WIFI tracker 1000109150
# allow access to DHCP server on WIFI
pass in  quick on $WIFI proto udp from any port = 68 to 255.255.255.255 port = 67 tracker 1000109171 label "allow access to DHCP server"
pass in  quick on $WIFI proto udp from any port = 68 to 192.168.2.1 port = 67 tracker 1000109172 label "allow access to DHCP server"
pass out  quick on $WIFI proto udp from 192.168.2.1 port = 67 to any port = 68 tracker 1000109173 label "allow access to DHCP server"

# loopback
pass in  on $loopback inet all tracker 1000110241 label "pass IPv4 loopback"
pass out  on $loopback inet all tracker 1000110242 label "pass IPv4 loopback"
# let out anything from the firewall host itself and decrypted IPsec traffic
pass out  inet all keep state allow-opts tracker 1000110243 label "let out anything IPv4 from firewall host itself"

pass out  route-to ( igb3 107.179.246.129 ) from 107.179.246.146 to !107.179.246.128/27 tracker 1000110341 keep state allow-opts label "let out anything from firewall host itself"
# make sure the user cannot lock himself out of the webConfigurator or SSH
pass in  quick on igb1 proto tcp from any to (igb1) port { 444 13267 } tracker 10000 keep state label "anti-lockout rule"
# NAT Reflection rules
pass in  inet tagged PFREFLECT tracker 1000110661 keep state label "NAT REFLECT: Allow traffic to localhost"

# User-defined rules follow

anchor "userrules/*"
pass  in  quick  on $OpenVPN inet from any to any tracker 1535475694 keep state  label "USER_RULE: OpenVPN OpenVPN tmntlair wizard"
pass  in  quick  on $WAN reply-to ( igb3 107.179.246.129 ) inet proto tcp  from any to $web_server port 443 tracker 1492787650 flags S/SA keep state  label "USER_RULE: NAT HTTPS redirect to web server"
pass  in  quick  on $WAN reply-to ( igb3 107.179.246.129 ) inet proto tcp  from any to $web_server port 80 tracker 1492787688 flags S/SA keep state  label "USER_RULE: NAT HTTP redirect to web server"
pass  in  quick  on $WAN reply-to ( igb3 107.179.246.129 ) inet proto { tcp udp }  from any to 192.168.1.10 port 32400 tracker 1496517247 keep state  label "USER_RULE: NAT Plex remote access"
pass  in  quick  on $LAN inet from 192.168.1.0/24 to any tracker 0100000101 keep state  label "USER_RULE: Default allow LAN to any rule"
# at the break! label "USER_RULE: Default allow LAN IPv6 to any rule"
pass  in  quick  on $LAN inet from 192.168.1.0/24 to $printers tracker 1458060516 keep state  label "USER_RULE: Allowing LAN to print."
pass  in  quick  on $WIFI inet from 192.168.2.0/24 to any tracker 1458059960 keep state  label "USER_RULE: Default allow OPT1 to any rule"
# at the break! label "USER_RULE: Default allow OPT1 IPv6 to any rule"
pass  in  quick  on $WIFI inet from $nvidia_shield_chromecast to 192.168.1.20 tracker 1458057456 keep state  label "USER_RULE: Allowing known OPT1 devices to talk to Unified Remot"
pass  in  quick  on $WIFI inet proto tcp  from $plex_access to 192.168.1.10 port 32400 tracker 1458057618 flags S/SA keep state  label "USER_RULE: Allowing known OPT1 devices to talk to Plex."
pass  in  quick  on $WIFI inet proto tcp  from $plex_access to 192.168.1.10 port 3005 tracker 1458057701 flags S/SA keep state  label "USER_RULE: Allowing known OPT1 devices to talk to Plex."
pass  in  quick  on $WIFI inet from $laptops to 192.168.1.0/24 tracker 1458060144 keep state  label "USER_RULE: Allows laptops to access LAN resources."
pass  in  quick  on $WIFI inet from 192.168.2.0/24 to $printers tracker 1458060687 keep state  label "USER_RULE: Allowing OPT1 to print."
pass  in  quick  on $WIFI inet from $wireless_access_points to 192.168.1.0/24 tracker 1484365404 keep state  label "USER_RULE: Allowing management of wireless access point."
pass  in  quick  on $WAN reply-to ( igb3 107.179.246.129 ) inet proto tcp  from any to $web_server port 15632 tracker 1519857136 flags S/SA keep state  label "USER_RULE: NAT web-server SSH forward."
pass  in  quick  on $WAN reply-to ( igb3 107.179.246.129 ) inet proto tcp  from any to $web_server port 4443 tracker 1527105070 flags S/SA keep state  label "USER_RULE: NAT 4443 redirect to web server"
pass  in  quick  on $WAN reply-to ( igb3 107.179.246.129 ) inet proto udp  from any to 107.179.246.146 port 1194 tracker 1535475693 keep state  label "USER_RULE: OpenVPN OpenVPN tmntlair wizard"

# VPN Rules

anchor "tftp-proxy/*"
anchor "miniupnpd"
