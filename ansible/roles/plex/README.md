# Purpose
Install and configure Plex Media Server.

In general, the server is not meant to be configured automatically. It was meant to be configured manually through the web app. That's mainly because every configs are stored inside a DB file and I don't really want to mess with that.

## OS
- Debian 9.5

## Configurations
For plex to find media, the permissions of media folders need to be valid, but that's not something I'm comfortable with automating:
    sudo find /media/nas/{Plex media folders} -type d -exec chmod 755 {} \;
    sudo find /media/nas/{Plex media folders} -type f -exec chmod 644 {} \;

## Backup
$PLEX_HOME/Library/Application Support/Plex Media Server/
or
/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/

Tip!: For Windows and Linux systems, you can exclude the `Cache` directory if you wish. That can save space and time in the transfer.

In Linux, the `preferences.xml` file in the main Plex Media Server directory contains the corresponding settings.

One last thing, the folder `Metadata` found within the installation path will grow to pretty big proportion (my library generates more than 66 GiB of data).
