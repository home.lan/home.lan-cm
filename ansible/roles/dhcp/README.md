# Purpose
Deploy a pre-configured DHCP server on the host.

## OS
- OpenBSD 6.4 (for the actual DHCP server)
- Debian 9.5 (for the dhclient)
