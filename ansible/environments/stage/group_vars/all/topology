---
# Vars in this section exists outside the main topology structure because a
# dict can't reference itself when being created.
#######################################
ip_netblock:
    LAN: 10.1.1
    WIFI: 10.2.2
    DMZ: 10.10.10

ip:
    router: "{{ ip_netblock.LAN }}.1"
    cm: "{{ ip_netblock.LAN }}.5"
    nas: "{{ ip_netblock.LAN }}.10"
    nextcloud: "{{ ip_netblock.DMZ }}.10"
    gitlab: "{{ ip_netblock.DMZ }}.11"
    workstation: "{{ ip_netblock.LAN }}.30"

interface:
    cm: enp0s3
    nas: enp0s3
    nextcloud: eth0
    gitlab: eth0
    workstation: eth0

domain_name: home.lan
#######################################

topology:
    domain_name: "{{ domain_name }}"
    domain_name_server: "tulkas.{{ domain_name }}"
    ntp_server: "tulkas.{{ domain_name }}"
    timezone: EST5EDT

    roles:
        router:
            hostname: tulkas
            default_interface: em0
        
            interfaces:
                em0:
                    name: WLAN
                    dhcp_client: true
                    dhcp_server: false
                    mac: 08:00:27:00:00:01
                em1:
                    name: LAN
                    dhcp_client: false
                    dhcp_server: true
                    ip: "{{ ip.router }}"
                    subnet: "{{ ip_netblock.LAN }}.0"
                    netblock: "{{ ip_netblock.LAN }}.0/24"
                    netmask: 255.255.255.0
                    broadcast_address: "{{ ip_netblock.LAN }}.255"
                    domain_name_servers: "{{ ip.router }}"
                    dhcp_range:
                        begin: "{{ ip_netblock.LAN }}.200"
                        end: "{{ ip_netblock.LAN }}.254"
                    mac: 08:00:27:01:01:01
                    fixed_address:
                        cm:
                            ip: "{{ ip.cm }}"
                            if: "{{ interface.cm }}"
                        nas:
                            ip: "{{ ip.nas }}"
                            if: "{{ interface.nas }}"
                        workstation:
                            ip: "{{ ip.workstation }}"
                            if: "{{ interface.workstation }}"
                em2:
                    name: WIFI
                    dhcp_client: false
                    dhcp_server: true
                    ip: "{{ ip_netblock.WIFI }}.1"
                    subnet: "{{ ip_netblock.WIFI }}.0"
                    netblock: "{{ ip_netblock.WIFI }}.0/24"
                    netmask: 255.255.255.0
                    broadcast_address: "{{ ip_netblock.WIFI }}.255"
                    domain_name_servers: "{{ ip_netblock.WIFI }}.1"
                    dhcp_range:
                        begin: "{{ ip_netblock.WIFI }}.200"
                        end: "{{ ip_netblock.WIFI }}.254"
                    mac: 08:00:27:02:02:01
                em3:
                    name: DMZ
                    dhcp_client: false
                    dhcp_server: true
                    ip: "{{ ip_netblock.DMZ }}.1"
                    subnet: "{{ ip_netblock.DMZ }}.0"
                    netblock: "{{ ip_netblock.DMZ }}.0/24"
                    netmask: 255.255.255.0
                    broadcast_address: "{{ ip_netblock.DMZ }}.255"
                    domain_name_servers: "{{ ip_netblock.DMZ }}.1"
                    dhcp_range:
                        begin: "{{ ip_netblock.DMZ }}.250"
                        end: "{{ ip_netblock.DMZ }}.254"
                    mac: 08:00:27:10:10:01
                    fixed_address:
                        nextcloud:
                            ip: "{{ ip.nextcloud }}"
                            if: "{{ interface.nextcloud }}"
                        gitlab:
                            ip: "{{ ip.gitlab }}"
                            if: "{{ interface.gitlab }}"

            ssh:
                users:
                    - { name: cm, groups: "wheel, ssh-users", password: "{{ router_ssh_user_cm_password | default('') }}" }
                    - { name: backup_nas, groups: "ssh-users", password: "{{ router_ssh_user_backup_nas_password | default('') }}" }
                    - { name: wolfmah, groups: "wheel, ssh-users", password: "{{ router_ssh_user_wolfmah_password | default('') }}" }
                allow_users:
                    - "cm@{{ ip.cm }}"
                    - "backup_nas@{{ ip.nas }}"
                    - "wolfmah@{{ ip.workstation }}"

            unbound:
                num_threads: 1
                cache_slabs: 2
                rrset_cache_size: 64m
                msg_cache_size: 32m

        cm:
            hostname: eru
            default_interface: "{{ interface.cm }}"

            interfaces:
                enp0s3:
                    dhcp_client: true
                    dhcp_server: false
                    mac: 08:00:27:01:01:05

            ssh:
                users:
                    - { name: wolfmah, groups: "sudo, ssh-users", password: "{{ cm_ssh_user_wolfmah_password | default('') }}" }
                allow_users:
                    - "wolfmah@{{ ip.workstation }}"

        nas:
            hostname: aule
            default_interface: "{{ interface.nas }}"

            interfaces:
                enp0s3:
                    dhcp_client: true
                    dhcp_server: false
                    mac: 08:00:27:01:01:10

            ssh:
                users:
                    - { name: cm, groups: "sudo, ssh-users", password: "{{ nas_ssh_user_cm_password | default('') }}" }
                    - { name: wolfmah, groups: "sudo, ssh-users", password: "{{ nas_ssh_user_wolfmah_password | default('') }}" }
                allow_users:
                    - "cm@{{ ip.cm }}"
                    - "wolfmah@{{ ip.workstation }}"

            data:
                user:
                    name: wolfmah
                    group: wolfmah
                    password: "{{ nas_samba_user_wolfmah_password | default('') }}"
                paths:
                    root: /media
                    pool: /media/data
                    drives: /media/wolfmah

        nextcloud:
            hostname: varda
            default_interface: "{{ interface.nextcloud }}"

            interfaces:
                eth0:
                    dhcp_client: true
                    dhcp_server: false
                    mac: 08:00:27:10:10:10

            ssh:
                users:
                    - { name: cm, groups: "sudo, ssh-users", password: "{{ nextcloud_ssh_user_cm_password | default('') }}" }
                    - { name: backup_nas, groups: "ssh-users", password: "{{ nextcloud_ssh_user_backup_nas_password | default('') }}" }
                    - { name: wolfmah, groups: "sudo, ssh-users", password: "{{ nextcloud_ssh_user_wolfmah_password | default('') }}" }
                allow_users:
                    - "cm@{{ ip.cm }}"
                    - "backup_nas@{{ ip.nas }}"
                    - "wolfmah@{{ ip.workstation }}"

        gitlab:
            hostname: lorien
            default_interface: "{{ interface.gitlab }}"

            interfaces:
                eth0:
                    dhcp_client: true
                    dhcp_server: false
                    mac: 08:00:27:10:10:11

            ssh:
                users:
                    - { name: cm, groups: "sudo, ssh-users", password: "{{ gitlab_ssh_user_cm_password | default('') }}" }
                    - { name: backup_nas, groups: "ssh-users", password: "{{ gitlab_ssh_user_backup_nas_password | default('') }}" }
                    - { name: wolfmah, groups: "sudo, ssh-users", password: "{{ gitlab_ssh_user_wolfmah_password | default('') }}" }
                allow_users:
                    - "cm@{{ ip.cm }}"
                    - "backup_nas@{{ ip.nas }}"
                    - "wolfmah@{{ ip.workstation }}"

        workstation:
            hostname: olorin
            default_interface: "{{ interface.workstation }}"

            interfaces:
                eth0:
                    dhcp_client: true
                    dhcp_server: false
                    mac: 08:00:27:01:01:30

            ssh:
                users:
                    - { name: cm, groups: "sudo, ssh-users", password: "{{ workstation_ssh_user_cm_password | default('') }}" }
                    - { name: backup_nas, groups: "ssh-users", password: "{{ workstation_ssh_user_backup_nas_password | default('') }}" }
                allow_users:
                    - "cm@{{ ip.cm }}"
                    - "backup_nas@{{ ip.nas }}"
