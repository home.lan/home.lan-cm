#!/usr/bin/env python3

#------------------------------------------------------------------------------
# Need to be run as root.
#
# OS: OpenBSD 6.4
# 
# - Setup a minimal SSH with root password allowed.
# - Setup a minimal pf configuration.
# - Setup a pretty complete unbound configuration.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Installation step-by-step
# Install
# Keyboard layout: default
# System hostname: tulkas
# Which network interface...?
#   em0
#       IPv4 address: dhcp
#       IPv6 address: none
#   done
# Password for root account: -----
# Start sshd(8) by default: yes
# X Window System: no
# Setup a user: no
# Allow root ssh login: no
# What timezone: EST5EDT
# Which disk it the root disk: wd0
# Whole
# Auto
# Location of sets: cd0
# Pathname to the sets: 6.4/amd64
# Set name(s): -g*
# Set name(s): -x*
# Set name(s): done
# Continue without verification: yes
# Location of sets: done
# Time appears wrong. Set to ...: yes
# Reboot
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Write those following lines in /root/bootstrap.sh:
#
# echo "https://openbsd.mirror.netelligent.ca/pub/OpenBSD" > /etc/installurl
# pkg_add curl python%3.6
# curl -o /tmp/tulkas.py https://gitlab.com/home.lan/home.lan-cm/raw/master/bootstrap/tulkas.py
# curl -o /tmp/common.py https://gitlab.com/home.lan/home.lan-cm/raw/master/bootstrap/common.py
# chmod +x /tmp/tulkas.py
# /tmp/tulkas.py vm
# if [ $? -eq 0 ]; then /tmp/template.sh; fi
#
# Save the VM image, then run this command:
#
# ./bootstrap.sh
#------------------------------------------------------------------------------

import common

def init_settings(environment):
    settings = {}

    settings["ENV_DNS_FORWARDING"] = "false"

    if environment == "vm_dns_forwarding":
        environment = "vm"
        settings["ENV_DNS_FORWARDING"] = "true"

    settings["DOMAIN_HOSTNAME"] = common.HOSTNAME_TULKAS
    settings["ENVIRONMENT"] = environment

    settings["DNS_ACCESS_CONTROL_NETBLOCK"] = "10.1.1.0\\/24"

    if environment == "vm":
        settings["NIC_INTERFACE_NAME_1"] = "em0"
        settings["NIC_INTERFACE_NAME_2"] = "em1"
        settings["NIC_INTERFACE_NAME_3"] = "em2"
        settings["NIC_INTERFACE_NAME_4"] = "em3"

        # Took it from /etc/master.passwd after creating a dumb user with the wanted password.
        settings["CM_PASSWORD"] = "\\\\$2b\\\\$10\\\\$a4ThpqcpGbdx8jOHUOtChu9RvQ6X5Yi4Wt5saRp1oFkcGkKjEbQ/C"

        settings["DNS_NUMBER_THREADS"] = "1"
        settings["DNS_NUMBER_SLABS"] = "2"
        settings["DNS_RRSET_CACHE_SIZE"] = "64m"
        settings["DNS_MSG_CACHE_SIZE"] = "32m"

        settings["MAC_ADDRESS_TULKAS_1"] = "08:00:27:00:00:01"
        settings["MAC_ADDRESS_TULKAS_2"] = "08:00:27:01:01:01"
        settings["MAC_ADDRESS_TULKAS_3"] = "08:00:27:02:02:01"
        settings["MAC_ADDRESS_TULKAS_4"] = "08:00:27:10:10:01"
        settings["MAC_ADDRESS_ERU"] = "08:00:27:01:01:05"
        settings["MAC_ADDRESS_AULE"] = "08:00:27:01:01:10"
        settings["MAC_ADDRESS_VARDA"] = "08:00:27:10:10:10"
        settings["MAC_ADDRESS_LORIEN"] = "08:00:27:10:10:11"
        settings["MAC_ADDRESS_OLORIN"] = "08:00:27:01:01:30"

        settings["SSH_ALLOW_USERS"] = "%s@%s" % (common.CM_USER, common.IP_ADDRESS_ERU)

    if environment == "metal":
        settings["NIC_INTERFACE_NAME_1"] = ""
        settings["NIC_INTERFACE_NAME_2"] = ""
        settings["NIC_INTERFACE_NAME_3"] = ""
        settings["NIC_INTERFACE_NAME_4"] = ""

        settings["CM_PASSWORD"] = ""

        settings["DNS_NUMBER_THREADS"] = "4"
        settings["DNS_NUMBER_SLABS"] = "8"
        settings["DNS_RRSET_CACHE_SIZE"] = "256m"
        settings["DNS_MSG_CACHE_SIZE"] = "128m"

        settings["MAC_ADDRESS_TULKAS"] = ""
        settings["MAC_ADDRESS_ERU"] = ""
        settings["MAC_ADDRESS_AULE"] = ""
        settings["MAC_ADDRESS_VARDA"] = ""
        settings["MAC_ADDRESS_LORIEN"] = ""
        settings["MAC_ADDRESS_OLORIN"] = ""

        print("Not implemented yet!")
        exit(1)

    return settings

if __name__ == "__main__":
    args = common.parse_args()
    common.run(init_settings(args.environment))
