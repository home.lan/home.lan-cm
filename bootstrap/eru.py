#!/usr/bin/env python3

#------------------------------------------------------------------------------
# Need to be run as root.
#
# OS: Debian 9.5.0
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Non graphical install
# Select a language: English
# Select your location: Canada
# Configure the keyboard: American English
# Hostname: eru
# Domain name: empty
# Root password: -----
# Set up users and passwords: test
# User password: -----
# Time zone: Eastern
# Partitioning method: Guided - use entire disk
# Select disk to partition: sda
# Partitioning scheme: All files in one partition
# Scan another CD: no
# Debian archive mirror country: Canada
# Debian archive mirror: whatever
# Proxy: none
# Participate in the package usage survey: no
# Software selection: none
# Install the GRUB boot loader to the master boot record: yes
# Device for boot loader installation: /dev/sda
# Reboot
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Write those following lines in /root/bootstrap.sh:
#
# apt install -y curl python3
# curl -o /tmp/eru.py https://gitlab.com/home.lan/home.lan-cm/raw/master/bootstrap/eru.py
# curl -o /tmp/common.py https://gitlab.com/home.lan/home.lan-cm/raw/master/bootstrap/common.py
# chmod +x /tmp/eru.py
# /tmp/eru.py vm
# if [ $? -eq 0 ]; then /tmp/template.sh; fi
#
# Save the VM image, then run this command:
#
# ./bootstrap.sh
#
# When completed, manually change .vault-pass value.
#------------------------------------------------------------------------------

import common
import crypt

def init_settings(environment):
    settings = {}

    settings["ENV_DNS_FORWARDING"] = "false"

    if environment == "vm_dns_forwarding":
        environment = "vm"
        settings["ENV_DNS_FORWARDING"] = "true"

    settings["DOMAIN_HOSTNAME"] = common.HOSTNAME_ERU
    settings["ENVIRONMENT"] = environment

    if environment == "vm":
        # Far from being secure, but I don't care for the VM...
        # import crypt; print crypt.crypt("test", "cm")
        settings["CM_PASSWORD"] = "cmgYwzZI1eHeE"

    if environment == "metal":
        settings["CM_PASSWORD"] = ""

        print("Not implemented yet!")
        exit(1)

    return settings

if __name__ == "__main__":
    args = common.parse_args()
    common.run(init_settings(args.environment))
