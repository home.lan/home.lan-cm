if [[ `whoami` == 'root' ]]; then
    # IF user is root

    ENV_DNS_FORWARDING=__ENV_DNS_FORWARDING__
    if [ "$ENV_DNS_FORWARDING" = true ] ; then
        touch /etc/ENV_DNS_FORWARDING
    fi

    # Update system
    apt update
    apt upgrade -y
    apt install -y man build-essential curl dirmngr git less libapt-pkg-dev libbz2-dev libsqlite3-dev libreadline-dev zlib1g-dev libncurses5-dev libssl-dev libgdbm-dev libncursesw5-dev libdb-dev libdb5.3-dev libexpat1-dev liblzma-dev libc6-dev tk-dev libffi-dev

    # Delete test user
    deluser --remove-home --remove-all-files test
    delgroup test

    # Config ssh_config
    curl -o /etc/ssh/ssh_config __GITLAB_BASE_URL__/bootstrap/files/ssh/ssh_config
    chown root:root /etc/ssh/ssh_config
    chmod 644 /etc/ssh/ssh_config

    # Install sudo
    apt install -y sudo

    curl -o /etc/sudoers __GITLAB_BASE_URL__/bootstrap/files/sudoers
    chown root:root /etc/sudoers
    chmod 440 /etc/sudoers

    # Add __CM_USER__ user
    groupadd ssh-users
    useradd -m -p __CM_PASSWORD__ -c __CM_USER__ -s /bin/bash -G sudo,ssh-users __CM_USER__

    echo ""
    echo "-------------------------------------------------------------------------------"
    echo "Done, now complete the setup by switching to __CM_USER__ user."
    echo "su - __CM_USER__"
    echo "/tmp/template.sh"
    exit 0
elif [[ `whoami` == '__CM_USER__' ]]; then
    # IF user is __CM_USER__

    # Installation that are better done outside of a Python script.
    PYTHON_VERSION="3.7.2"
    PYTHON_PATH_BASEDIR="/home/cm/python"
    PYTHON_PATH="${PYTHON_PATH_BASEDIR}/python-${PYTHON_VERSION}"
    PYTHON_URL="https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz"

    PYTHON_APT_VERSION="1.6.2"
    PYTHON_APT_PATH="${PYTHON_PATH_BASEDIR}/python-apt-${PYTHON_APT_VERSION}"
    PYTHON_APT_URL="https://salsa.debian.org/apt-team/python-apt"

    ANSIBLE_VENV="/home/cm/ansible"

    ANSIBLE_VERSION="2.7.5"
    ANSIBLE_VERSION_FULL="v${ANSIBLE_VERSION}"
    ANSIBLE_BASEDIR="${ANSIBLE_VENV}/src"
    ANSIBLE_PATH="${ANSIBLE_VENV}/src/ansible-${ANSIBLE_VERSION}"
    ANSIBLE_URL="https://github.com/ansible/ansible.git"

    HOME_LAN_PATH="/home/cm/home.lan-cm"

    # Install a local Python
    mkdir -p ${PYTHON_PATH_BASEDIR}
    cd ${PYTHON_PATH_BASEDIR}
    curl -o Python-${PYTHON_VERSION}.tar.xz https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz
    tar xf Python-${PYTHON_VERSION}.tar.xz
    mv Python-${PYTHON_VERSION} python-${PYTHON_VERSION}
    rm Python-${PYTHON_VERSION}.tar.xz
    cd ${PYTHON_PATH}
    ./configure --prefix=${PYTHON_PATH}
    if [ $? != 0 ]; then echo "ERROR: Python ./configure --prefix=${PYTHON_PATH}"; exit 1; fi
    make
    if [ $? != 0 ]; then echo "ERROR: Python make"; exit 1; fi
    make install
    if [ $? != 0 ]; then echo "ERROR: Python make install"; exit 1; fi

    # Setup Python virtual environement
    # Install python-apt
    # Install Ansible
    #
    # Since Ansible will be running from a Python venv, there is no default way
    # to talk to APT. This is where this manual build of the `python-apt` lib
    # is, the glue between the venv and the global system (for APT).
    ${PYTHON_PATH}/bin/python3 -m venv ${ANSIBLE_VENV}
    git clone ${PYTHON_APT_URL} ${PYTHON_APT_PATH}
    cd ${PYTHON_APT_PATH}
    # Check versions via `git tag`.
    git checkout ${PYTHON_APT_VERSION}
    (
        source ${ANSIBLE_VENV}/bin/activate

        pip3 install --upgrade pip
        pip3 install --upgrade passlib bcrypt
        python3 setup.py build
        python3 setup.py install

        deactivate
    )

    # Install Ansible
    mkdir ${ANSIBLE_BASEDIR}
    git clone ${ANSIBLE_URL} --recursive ${ANSIBLE_PATH}
    cd ${ANSIBLE_PATH}
    git checkout ${ANSIBLE_VERSION_FULL}
    curl -o ${ANSIBLE_PATH}/hacking/env-setup __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/ansible/env-setup
    (
        source ${ANSIBLE_VENV}/bin/activate

        source ./hacking/env-setup
        pip3 install -r ./requirements.txt

        deactivate
    )

    mkdir -p ~/bin
    echo "export ANSIBLE_HOME=\"${ANSIBLE_PATH}\"" > ~/bin/ansible.in
    echo "export PATH=\"\$PATH:\$ANSIBLE_HOME/bin\"" >> ~/bin/ansible.in
    echo "export PYTHONPATH=\"\$PYTHONPATH:\$ANSIBLE_HOME/lib\"" >> ~/bin/ansible.in
    echo "export MANPATH=\"\$MANPATH:\$ANSIBLE_HOME/docs/man\"" >> ~/bin/ansible.in

    for EXEC_NAME in ansible-config ansible-connection ansible-console ansible-doc ansible-galaxy ansible-inventory ansible-playbook ansible-pull ansible-test ansible-vault ansible; do
        echo "#!/bin/bash" > ~/bin/${EXEC_NAME}.sh
        echo "source \"/home/cm/ansible/bin/activate\"" >> ~/bin/${EXEC_NAME}.sh
        echo "source \"/home/cm/bin/ansible.in\"" >> ~/bin/${EXEC_NAME}.sh
        echo "\${ANSIBLE_HOME}/bin/${EXEC_NAME} \$@" >> ~/bin/${EXEC_NAME}.sh
        echo "deactivate" >> ~/bin/${EXEC_NAME}.sh
    done
    chmod 700 ~/bin/*

    # Add Ansible project
    git clone https://gitlab.com/home.lan/home.lan-cm.git ${HOME_LAN_PATH}
    echo "test" > ${HOME_LAN_PATH}/ansible/.vault-pass
    chmod 600 ${HOME_LAN_PATH}/ansible/.vault-pass
    curl -o ${HOME_LAN_PATH}/ansible/ansible-playbook.sh __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/ansible-playbook.sh
    chmod +x ${HOME_LAN_PATH}/ansible/ansible-playbook.sh

    # Add SSH keys
    if [ ! -d ~/.ssh ]; then
        mkdir ~/.ssh
        chmod 700 ~/.ssh
    fi
    curl -o ~/.ssh/cm-cm-to-router __GITLAB_BASE_URL__/bootstrap/files/ssh/fake-keys/__ENVIRONMENT__/cm-cm-to-router
    chmod 600 ~/.ssh/cm-cm-to-router

    # Add SSH fingerprints
    if [ ! -f ~/.ssh/known_hosts ]; then
        touch ~/.ssh/known_hosts
        chmod 600 ~/.ssh/known_hosts
    fi
    ssh-keyscan -H -t rsa __HOSTNAME_TULKAS__.__DOMAIN_NAME__ >> ~/.ssh/known_hosts

    # Done
    sudo rm /root/bootstrap.sh
    sudo rm /tmp/common.py
    sudo rm /tmp/__DOMAIN_HOSTNAME__.py
    sudo rm /tmp/template.sh
    sudo reboot
fi
