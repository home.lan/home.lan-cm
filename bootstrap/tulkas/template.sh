ENV_DNS_FORWARDING=__ENV_DNS_FORWARDING__
if [ "$ENV_DNS_FORWARDING" = true ] ; then
    touch /etc/ENV_DNS_FORWARDING
fi

# Ensure hostname is OK
echo "__DOMAIN_FQDN__" > /etc/myname

# Patch system with securty fixes
syspatch

# Add packages
pkg_add nano

# Add users and groups
curl -o /etc/adduser.conf __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/adduser.conf
chown root:wheel /etc/adduser.conf
chmod 644 /etc/adduser.conf

groupadd __CM_USER__
groupadd ssh-users
adduser -batch __CM_USER__ __CM_USER__,wheel,ssh-users __CM_USER__ __CM_PASSWORD__

if [ ! -f /home/__CM_USER__/.ssh/authorized_keys ]; then
    touch /home/__CM_USER__/.ssh/authorized_keys
    chmod 600 /home/__CM_USER__/.ssh/authorized_keys
    chown __CM_USER__:__CM_USER__ /home/__CM_USER__/.ssh/authorized_keys
fi

curl __GITLAB_BASE_URL__/bootstrap/files/ssh/fake-keys/__ENVIRONMENT__/cm-cm-to-router.pub >> /home/__CM_USER__/.ssh/authorized_keys

# Enable doas for __CM_USER__ user
echo "permit nopass __CM_USER__ as root" > /etc/doas.conf

# Install python
pkg_add python%3.6 py3-pip
ln -sf /usr/local/bin/python3.6 /usr/local/bin/python3
ln -sf /usr/local/bin/2to3-3.6 /usr/local/bin/2to3
ln -sf /usr/local/bin/python3.6m /usr/local/bin/python3m
ln -sf /usr/local/bin/python3.6m-config /usr/local/bin/python3m-config
ln -sf /usr/local/bin/pyvenv-3.6 /usr/local/bin/pyvenv-3
ln -sf /usr/local/bin/easy_install-3.6 /usr/local/bin/easy_install-3
ln -sf /usr/local/bin/pip3.6 /usr/local/bin/pip3
pip3 install --upgrade pip

# Enable minimal ssh
rcctl enable sshd
curl -o /etc/ssh/ssh_config __GITLAB_BASE_URL__/bootstrap/files/ssh/ssh_config
curl -o /etc/ssh/sshd_config __GITLAB_BASE_URL__/bootstrap/files/ssh/sshd_config
chown root:wheel /etc/ssh/ssh_config
chown root:wheel /etc/ssh/sshd_config
chmod 644 /etc/ssh/ssh_config
chmod 644 /etc/ssh/sshd_config
sed -i "s/--SSH_ALLOW_USERS--/__SSH_ALLOW_USERS__/g" /etc/ssh/sshd_config

# Ensure network is OK
echo "net.inet.ip.forwarding=1" >> /etc/sysctl.conf
chown root:wheel /etc/sysctl.conf
chmod 644 /etc/sysctl.conf

echo "__IP_ADDRESS_TULKAS_1__" > /etc/hostname.__NIC_INTERFACE_NAME_1__
chown root:wheel /etc/hostname.__NIC_INTERFACE_NAME_1__
chmod 640 /etc/hostname.__NIC_INTERFACE_NAME_1__
echo "inet __IP_ADDRESS_TULKAS_2__ 255.255.255.0" > /etc/hostname.__NIC_INTERFACE_NAME_2__
chown root:wheel /etc/hostname.__NIC_INTERFACE_NAME_2__
chmod 640 /etc/hostname.__NIC_INTERFACE_NAME_2__
echo "inet __IP_ADDRESS_TULKAS_3__ 255.255.255.0" > /etc/hostname.__NIC_INTERFACE_NAME_3__
chown root:wheel /etc/hostname.__NIC_INTERFACE_NAME_3__
chmod 640 /etc/hostname.__NIC_INTERFACE_NAME_3__
echo "inet __IP_ADDRESS_TULKAS_4__ 255.255.255.0" > /etc/hostname.__NIC_INTERFACE_NAME_4__
chown root:wheel /etc/hostname.__NIC_INTERFACE_NAME_4__
chmod 640 /etc/hostname.__NIC_INTERFACE_NAME_4__

# DHCP
rcctl enable dhcpd
rcctl set dhcpd flags __NIC_INTERFACE_NAME_2__ __NIC_INTERFACE_NAME_3__ __NIC_INTERFACE_NAME_4__
curl -o /etc/dhcpd.conf __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/dhcpd.conf
chown root:wheel /etc/dhcpd.conf
chmod 644 /etc/dhcpd.conf
sed -i "s/--DOMAIN_NAME--/__DOMAIN_NAME__/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_TULKAS_2--/__IP_ADDRESS_TULKAS_2__/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_ERU--/__IP_ADDRESS_ERU__/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_AULE--/__IP_ADDRESS_AULE__/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_VARDA--/__IP_ADDRESS_VARDA__/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_LORIEN--/__IP_ADDRESS_LORIEN__/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_OLORIN--/__IP_ADDRESS_OLORIN__/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_TULKAS_2--/__MAC_ADDRESS_TULKAS_2__/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_ERU--/__MAC_ADDRESS_ERU__/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_AULE--/__MAC_ADDRESS_AULE__/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_VARDA--/__MAC_ADDRESS_VARDA__/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_LORIEN--/__MAC_ADDRESS_LORIEN__/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_OLORIN--/__MAC_ADDRESS_OLORIN__/g" /etc/dhcpd.conf

curl -o /etc/dhclient.conf __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/dhclient.conf
chown root:wheel /etc/dhclient.conf
chmod 644 /etc/dhclient.conf
sed -i "s/--NIC_INTERFACE_NAME_1--/__NIC_INTERFACE_NAME_1__/g" /etc/dhclient.conf

# Firewall/NAT
rcctl enable pf
curl -o /etc/pf.conf __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/pf.conf
chown root:wheel /etc/pf.conf
chmod 600 /etc/pf.conf
sed -i "s/--NIC_INTERFACE_NAME_2--/__NIC_INTERFACE_NAME_2__/g" /etc/pf.conf

# DNS
rcctl enable unbound

curl -o /var/unbound/etc/root.hints __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/root.hints
chown root:wheel /var/unbound/etc/root.hints
chmod 644 /var/unbound/etc/root.hints

if [ ! -f /etc/ENV_DNS_FORWARDING ]; then
    curl -o /var/unbound/db/root.key __GITLAB_BASE_URL__/ansible/roles/unbound/files/root.key
    chown root:_unbound /var/unbound/db/root.key
    chmod 664 /var/unbound/db/root.key
fi

if [ ! -f /etc/ENV_DNS_FORWARDING ]; then
    curl -o /var/unbound/etc/unbound.conf __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/unbound.conf
else
    curl -o /var/unbound/etc/unbound.conf __GITLAB_BASE_URL__/bootstrap/__DOMAIN_HOSTNAME__/files/unbound_ENV_DNS_FORWARDING.conf
fi
chown root:wheel /var/unbound/etc/unbound.conf
chmod 644 /var/unbound/etc/unbound.conf
sed -i "s/--DNS_NUMBER_THREADS--/__DNS_NUMBER_THREADS__/g" /var/unbound/etc/unbound.conf
sed -i "s/--DNS_NUMBER_SLABS--/__DNS_NUMBER_SLABS__/g" /var/unbound/etc/unbound.conf
sed -i "s/--DNS_RRSET_CACHE_SIZE--/__DNS_RRSET_CACHE_SIZE__/g" /var/unbound/etc/unbound.conf
sed -i "s/--DNS_MSG_CACHE_SIZE--/__DNS_MSG_CACHE_SIZE__/g" /var/unbound/etc/unbound.conf
sed -i "s/--DOMAIN_NAME--/__DOMAIN_NAME__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_TULKAS_1--/__IP_ADDRESS_TULKAS_1__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_TULKAS_2--/__IP_ADDRESS_TULKAS_2__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_TULKAS_3--/__IP_ADDRESS_TULKAS_3__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_TULKAS_4--/__IP_ADDRESS_TULKAS_4__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_ERU--/__IP_ADDRESS_ERU__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_AULE--/__IP_ADDRESS_AULE__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_VARDA--/__IP_ADDRESS_VARDA__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_LORIEN--/__IP_ADDRESS_LORIEN__/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_OLORIN--/__IP_ADDRESS_OLORIN__/g" /var/unbound/etc/unbound.conf
sed -i "s/--DNS_ACCESS_CONTROL_NETBLOCK--/__DNS_ACCESS_CONTROL_NETBLOCK__/g" /var/unbound/etc/unbound.conf

# Done
rm /root/bootstrap.sh
rm /tmp/common.py
rm /tmp/__DOMAIN_HOSTNAME__.py
rm /tmp/template.sh
reboot
