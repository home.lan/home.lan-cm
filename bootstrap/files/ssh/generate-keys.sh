if [ "$1" != "vm" ] && [ "$1" != "metal" ]; then
    echo "ERROR $1: missing vm or metal"
    exit 1
fi

if [ "$2" != "fake" ] && [ "$2" != "real" ]; then
    echo "ERROR $2: missing fake or real"
    exit 1
fi

mkdir --parents ./$2-keys/$1

if [ "$2" == "fake" ]; then
    ssh-keygen -t rsa -b 4096 -N "" -C "cm-cm-to-router" -f ./$2-keys/$1/cm-cm-to-router
fi

if [ "$2" == "real" ]; then
    echo "cm-cm-to-router passphrase:"
    ssh-keygen -t rsa -b 4096 -C "cm-cm-to-router" -f ./$2-keys/$1/cm-cm-to-router
fi
