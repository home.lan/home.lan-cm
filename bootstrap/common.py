import argparse
import os
import re
import shutil
import tempfile
import urllib.request
import urllib.error

GITLAB_BASE_URL = "https://gitlab.com/home.lan/home.lan-cm/raw/master"

DOMAIN_SECOND_LEVEL = "home"
DOMAIN_TOP_LEVEL = "lan"
DOMAIN_NAME = "%s.%s" % (DOMAIN_SECOND_LEVEL, DOMAIN_TOP_LEVEL)

CM_USER = "cm"

IP_ADDRESS_TULKAS_1 = "dhcp"
IP_ADDRESS_TULKAS_2 = "10.1.1.1"
IP_ADDRESS_TULKAS_3 = "10.2.2.1"
IP_ADDRESS_TULKAS_4 = "10.10.10.1"
IP_ADDRESS_ERU = "10.1.1.5"
IP_ADDRESS_AULE = "10.1.1.10"
IP_ADDRESS_VARDA = "10.10.10.10"
IP_ADDRESS_LORIEN = "10.10.10.11"
IP_ADDRESS_OLORIN = "10.1.1.30"

HOSTNAME_TULKAS = "tulkas"
HOSTNAME_ERU = "eru"
HOSTNAME_AULE = "aule"
HOSTNAME_VARDA = "varda"
HOSTNAME_LORIEN = "lorien"
HOSTNAME_OLORIN = "olorin"

def parse_args():
    parser = argparse.ArgumentParser(description="Bootstrap tulkas, an OpenBSD router.")

    parser.add_argument(
        "environment",
        choices=["vm", "vm_dns_forwarding", "metal"])

    return parser.parse_args()

def download_file(url, destination_file):
    file = urllib.request.urlopen(url)

    with open(destination_file, "wb") as local_file:
        local_file.write(file.read())

def sed_inplace(filename, pattern, repl):
    '''
    Perform the pure-Python equivalent of in-place `sed` substitution: e.g.,
    `sed -i -e 's/'${pattern}'/'${repl}' "${filename}"`.
    '''
    # For efficiency, precompile the passed regular expression.
    pattern_compiled = re.compile(pattern)

    # For portability, NamedTemporaryFile() defaults to mode "w+b" (i.e.,
    # binary writing with updating). This is usually a good thing. In this
    # case, however, binary writing imposes non-trivial encoding constraints
    # trivially resolved by switching to text writing. Let's do that.
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as tmp_file:
        with open(filename) as src_file:
            for line in src_file:
                tmp_file.write(pattern_compiled.sub(repl, line))

    # Overwrite the original file with the munged temporary file in a manner
    # preserving file attributes (e.g., permissions).
    shutil.copystat(filename, tmp_file.name)
    shutil.move(tmp_file.name, filename)

def run(settings):
    settings["GITLAB_BASE_URL"] = GITLAB_BASE_URL
    settings["DOMAIN_NAME"] = DOMAIN_NAME
    settings["DOMAIN_FQDN"] = "%s.%s" % (settings["DOMAIN_HOSTNAME"], settings["DOMAIN_NAME"])

    settings["CM_USER"] = CM_USER

    settings["IP_ADDRESS_TULKAS_1"] = IP_ADDRESS_TULKAS_1
    settings["IP_ADDRESS_TULKAS_2"] = IP_ADDRESS_TULKAS_2
    settings["IP_ADDRESS_TULKAS_3"] = IP_ADDRESS_TULKAS_3
    settings["IP_ADDRESS_TULKAS_4"] = IP_ADDRESS_TULKAS_4
    settings["IP_ADDRESS_ERU"] = IP_ADDRESS_ERU
    settings["IP_ADDRESS_AULE"] = IP_ADDRESS_AULE
    settings["IP_ADDRESS_VARDA"] = IP_ADDRESS_VARDA
    settings["IP_ADDRESS_LORIEN"] = IP_ADDRESS_LORIEN
    settings["IP_ADDRESS_OLORIN"] = IP_ADDRESS_OLORIN

    settings["HOSTNAME_TULKAS"] = HOSTNAME_TULKAS
    settings["HOSTNAME_ERU"] = HOSTNAME_ERU
    settings["HOSTNAME_AULE"] = HOSTNAME_AULE
    settings["HOSTNAME_VARDA"] = HOSTNAME_VARDA
    settings["HOSTNAME_LORIEN"] = HOSTNAME_LORIEN
    settings["HOSTNAME_OLORIN"] = HOSTNAME_OLORIN

    download_file(
        "%s/bootstrap/%s/template.sh" % (settings["GITLAB_BASE_URL"], settings["DOMAIN_HOSTNAME"]),
        "/tmp/template.sh")
    os.chmod("/tmp/template.sh", 0o755)

    for key, value in settings.items():
        sed_inplace("/tmp/template.sh", "__%s__" % key, value)

    print("")
