# home.lan-cm

Configuration management scripts for maintaining my home network of servers. The software use to do the work is [Ansible](https://docs.ansible.com/).

**Note**: None of the configuration done here are meant to scale beyond one host per role, i.e.: the group `router` only have one host, same goes for `nas` or `email`. For my home network, I don't need that level of complexity. Which means, the group `router` have one host: `tulkas`, and only that one. I will define everything by their roles instead of their hostname.

## Vagrant

All internal tests (staging) are done via [Vagrant](https://www.vagrantup.com/docs/) boxes that [I have created](https://app.vagrantup.com/wolfmah). I haven't used already available boxes, because I wanted to know to do them and make sure they have the bare minimum configuration required to work with Vagrant.

## Ansible installation

Ansible is on the configuration management (CM) machine, name `eru`. The host have Debian stable installed on it, but for Ansible, I am not using the package found in Debian's repo, I'm using the latest stable branch straight from GitHub: [stable-2.7](https://github.com/ansible/ansible/tree/stable-2.7).

Also, I am using the latest Python 3.x to launch Ansible. The latest Python is installed in the user `cm` folders, inside a virtual environment. Ansible is installed in the virtual environment too.

A script exists to do the installation/upgrade of Python, python-apt and Ansible: [scripts/update-python-ansible.sh](scripts/update-python-ansible.sh).

## Usage

The basic network comprise the router and configuration management (CM) machine. When starting from scratch (no server installed), those two machines will require special treatment to bootstrap; can't do it via Ansible, as their is no CM machine and no network.

To bootstrap those two machine, they need to be done in a specific order: router than CM. The bootstrapping steps are as followed:

* Router
    1. Manually install the router. Answers to the installation questions can be found in the header comment of [bootstrap/tulkas.py](bootstrap/tulkas.py).
    1. Manually create a shell script by following the description found in the header comment of [bootstrap/tulkas.py](bootstrap/tulkas.py).
    1. Execute the shell script.
* CM
    1. Manually install the CM. Answers to the installation questions can be found in the header comment of [bootstrap/eru.py](bootstrap/eru.py).
    1. Manually create a shell script by following the description found in the header comment of [bootstrap/eru.py](bootstrap/eru.py).
    1. Execute the shell script.
    1. Log off, log in as the user `cm` and resume the execution.

Now, the bootstrapping of the two servers is done. It is now possible to provision them with appropriate configurations. All the steps now can be done via Ansible.

1. On CM, log with user `cm` and `cd` into `~/home.lan-cm/ansible`.
    1. First, CM needs to be provisioned before it can execute proper Ansible commands: `ansible-playbook.sh provision-cm.yml`
    1. Then, the network can be provisioned with: `ansible-playbook.sh playbook-network.yml`

With that, the most basic network of servers is configured and ready to roll. When adding new servers to the mix, update the appropriate Ansible roles/playbooks/scripts/vars and do the following (example is done with a nas/Debian host):

1. On the newly installed host, log with `root`
    1. `apt install -y openssh-server openssh-client`
    1. `echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config`
    1. `systemctl restart sshd`
1. On CM, log with user `cm` and `cd` into `~/home.lan-cm/ansible`
    1. `ansible-playbook.sh playbook-network.yml`
    1. `ansible-playbook.sh -k bootstrap-nas.yml`
    1. `ansible-playbook.sh home.lan.yml`

The bootstrapping of any other server, other than router and CM, can be done in any order.

The whole network can be be kept maintained with: `ansible-playbook.sh home.lan.yml`.

## Annex

### Hard drives

This is mostly in relation to the following roles: fstab, snapraid, smartd.

For the server `NAS`, I use a strict nomenclature. Parity drives are named `PARITY_#` and data drives are named `CONTENT_#`. The `#` is a sequential number, starting at 1 for both.

#### Partionning

The following are manual steps to be performed when a new drive is to be added to `NAS`.

1. Start parted as follows:

       sudo parted /dev/sdX

1. Create a new GPT disklabel (aka partition table):

       (parted) mklabel gpt

1. Set the default unit to TB:

       (parted) unit TB

1. Create one partition occupying all the space on the drive:

       (parted) mkpart primary 0 0

1. Check that the results are correct:

       (parted) print

1. Save and quit `parted`:

       (parted) quit

#### Formatting

To format the new partition as ext4 file system:

    sudo mke2fs -t ext4 -L PARITY_1 -U 12345678-90ab-cdef-1234-5678900000## /dev/sdX#

I use a manual UUID to more easily differentiate between the drives. The `##` is a sequential number to be incremented to each new drive. This helps with the testing in virtual environment vs production.

### What I learned

During my time working with Ansible and provisioning / maintaining my servers, this is a list of all the stuff that I have touched on, most are new.

* Ansible
    * YAML
    * Jinja2
    * Python 3.x
    * Installation
        * How to download and compile the latest Python 3.x.
        * How to install a Python virtual environment (venv) from the newly compile Python 3.x.
        * How to install PIP modules to the Python venv.
        * How to download and compile the latest Ansible.
        * How to install Ansible inside the Python venv.
        * How to execute Ansible via the Python 3.x venv instead of the default Python 2.x found on the system.
        * All the above, in the user space, not via root.
        * All the above scripted via a Bash script.
* SSH
    * Configuration
    * Keys management
* OpenBSD
    * Everything is familiar to GNU/Linux, yet nothing works the same.
    * How to configure:
        * doas (sudo)
        * pf (packet filtering rules; iptables)
        * unbound (dns resolver)
        * dhcpd/dhclient (dhcp server/client configuration)
        * ntf (time server configuration)
        * Manage multiple NICs
* Debian
    * How to manage Debian GNU/Linux distro, which is different enough than Ubuntu, another distro I know well.
* nut
    * UPS management across multiple servers.
    * How to install the master on OpenBSD.
    * How to configure all the slaves installed on other OS.
    * Everything properly secured via specific users/passwords.
* User management
    * How to generate password safely via Ansible.
        * How to store plain text password in Ansible Vault.
        * How to generate a proper hash from the plain text password; very different methods used for Debian and OpenBSD.
        * How to make all generated password idempotent via Ansible without writing them to disk.
* Samba server/shares
* Monitoring
    * Hard drive health
* SnapRAID
    * Software raid for managing lots of data on hard drives.
* Plex
    * How to automate the installation and upgrade process (which is a highly manual process with no thought on automation).
