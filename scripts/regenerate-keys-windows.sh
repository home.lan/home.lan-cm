#!/bin/bash
#
# Helper script when developping on Windows with Windows subsystem for Linux
# (WSL).

###############################################################################
# Bash specific
# Get absolut path of the current script directory.
SOURCE="${BASH_SOURCE[0]}"
# Resolve $SOURCE until the file is no longer a symlink.
while [ -h "$SOURCE" ]; do
    DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    # If $SOURCE was a relative symlink, we need to resolve it relative to the
    # path where the symlink file was located.
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
###############################################################################

DIR_PARENT=$(dirname "$DIR")
ORIGINAL_CWD=$(pwd)
PASSWORD_FILENAME="regenerate-keys-windows-password"

SSH_KEY_NAMES="\
    backup_nas-nas-to-gitlab \
    backup_nas-nas-to-workstation \
    backup_nas-nas-to-router \
    backup_nas-nas-to-nextcloud \
    cm-cm-to-nas cm-cm-to-gitlab \
    cm-cm-to-workstation \
    cm-cm-to-router \
    cm-cm-to-nextcloud \
    wolfmah-workstation-to-nas \
    wolfmah-workstation-to-cm \
    wolfmah-workstation-to-gitlab \
    wolfmah-workstation-to-router \
    wolfmah-workstation-to-nextcloud"

# Check if the file containing the Ansible encryption password exists.
if [ ! -f "$DIR_PARENT/scripts/$PASSWORD_FILENAME" ]; then
    echo "ERROR: The password file doesn't exists: $DIR_PARENT/scripts/$PASSWORD_FILENAME"
    exit 1
fi

# Get the Ansible encryption password.
. "$DIR_PARENT/scripts/$PASSWORD_FILENAME"
if [ -z $VAULT_PASS ]; then
    echo "ERROR: Vault password is empty. Set a string to VAULT_PASS inside the file: $DIR_PARENT/misc/$PASSWORD_FILENAME"
    exit 1
fi

# Generate the SSH keys.
cd "$DIR_PARENT/ansible/roles/ssh-keys/files/"
rm -rf *
mkdir authorized_keys
for KEY_NAME in ${SSH_KEY_NAMES}; do
    echo "[INFO] Generate the SSH key ${KEY_NAME}..."
    ssh-keygen -t ed25519 -a 100 -o -q -N '' -C "${KEY_NAME}" -f ${KEY_NAME}
done

# Fill the authorized_keys files.
echo "[INFO] Fill the authorized_keys files..."
for KEY_NAME in ${SSH_KEY_NAMES}; do
    # The last bit (##*-) is a bash trick to substring from the last match (in
    # this case -) to the end of the string.
    # Ex.: backup_nas-nas-to-gitlab will result in gitlab
    cat ${KEY_NAME}.pub >> authorized_keys/${KEY_NAME##*-}
done

# Make a local copy, inside WSL, otherwise ansible-vault won't launch.
cd ~
rm -rf home.lan
mkdir home.lan
chmod 700 home.lan
cd home.lan
cp "$DIR_PARENT/ansible/ansible.cfg" .
echo "$VAULT_PASS" > .vault-pass

# Encrypt the SSH private keys.
for KEY_NAME in ${SSH_KEY_NAMES}; do
    echo "[INFO] Encrypt the SSH private key ${KEY_NAME}..."
    ansible-vault encrypt "$DIR_PARENT/ansible/roles/ssh-keys/files/${KEY_NAME}"
done

cd "$ORIGINAL_CWD"
