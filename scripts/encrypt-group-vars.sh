#!/bin/bash
#
# Helper script when developping on Windows with Windows subsystem for Linux
# (WSL).

###############################################################################
# Bash specific
# Get absolut path of the current script directory.
SOURCE="${BASH_SOURCE[0]}"
# Resolve $SOURCE until the file is no longer a symlink.
while [ -h "$SOURCE" ]; do
    DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    # If $SOURCE was a relative symlink, we need to resolve it relative to the
    # path where the symlink file was located.
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
###############################################################################

DIR_PARENT=$(dirname "$DIR")
ORIGINAL_CWD=$(pwd)
PASSWORD_FILENAME="encrypt-group-vars-password"

# Check if the file containing the Ansible encryption password exists.
if [ ! -f "$DIR_PARENT/scripts/$PASSWORD_FILENAME" ]; then
    echo "ERROR: The password file doesn't exists: $DIR_PARENT/scripts/$PASSWORD_FILENAME"
    exit 1
fi

# Get the Ansible encryption password.
. "$DIR_PARENT/scripts/$PASSWORD_FILENAME"
if [ -z $VAULT_PASS ]; then
    echo "ERROR: Vault password is empty. Set a string to VAULT_PASS inside the file: $DIR_PARENT/misc/$PASSWORD_FILENAME"
    exit 1
fi

DECRYPT="false"

if [[ "$1" == "decrypt" ]]; then
    DECRYPT="true"
fi

# Make a local copy, inside WSL, otherwise ansible-vault won't launch.
cd ~
rm -rf home.lan
mkdir home.lan
chmod 700 home.lan
cd home.lan
cp "$DIR_PARENT/ansible/ansible.cfg" .
echo "$VAULT_PASS" > .vault-pass

# Encrypt the vault files.
for VAULT_PATH in "$DIR_PARENT/ansible/environments/stage/group_vars/all/vault" \
                  "$DIR_PARENT/ansible/environments/stage/group_vars/cm/vault" \
                  "$DIR_PARENT/ansible/environments/stage/group_vars/gitlab/vault" \
                  "$DIR_PARENT/ansible/environments/stage/group_vars/nas/vault" \
                  "$DIR_PARENT/ansible/environments/stage/group_vars/nextcloud/vault" \
                  "$DIR_PARENT/ansible/environments/stage/group_vars/router/vault" \
                  "$DIR_PARENT/ansible/environments/stage/group_vars/workstation/vault"; do
    # Check if vault is already encrypted.
    head -1 "$VAULT_PATH" | grep -q "^\$ANSIBLE_VAULT";

    if [[ $? == 0 && "$DECRYPT" == "true" ]]; then
        ansible-vault decrypt "$VAULT_PATH"
    elif [[ $? != 0 && "$DECRYPT" == "false" ]]; then
        ansible-vault encrypt "$VAULT_PATH"
    else
        echo "Mismatch of encrypt/decrypt: $VAULT_PATH"
    fi
done

cd "$ORIGINAL_CWD"
