#!/bin/bash

PYTHON_VERSION="3.7.2"
PYTHON_PATH_BASEDIR="${HOME}/python"
PYTHON_PATH="${PYTHON_PATH_BASEDIR}/python-${PYTHON_VERSION}"
PYTHON_URL="https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz"

# More recent versions of Python APT spew errors (tested 1.6.3 and 1.7.0).
PYTHON_APT_VERSION="1.6.2"
PYTHON_APT_PATH="${PYTHON_PATH_BASEDIR}/python-apt-${PYTHON_APT_VERSION}"
PYTHON_APT_URL="https://salsa.debian.org/apt-team/python-apt"

ANSIBLE_VENV="${HOME}/ansible"

ANSIBLE_VERSION="2.7.5"
ANSIBLE_VERSION_FULL="v${ANSIBLE_VERSION}"
ANSIBLE_BASEDIR="${ANSIBLE_VENV}/src"
ANSIBLE_PATH="${ANSIBLE_VENV}/src/ansible-${ANSIBLE_VERSION}"
ANSIBLE_URL="https://github.com/ansible/ansible.git"

HOME_LAN_PATH="${HOME}/home.lan-cm"

NEW_PYTHON_BUILD=false
NEW_PYTHON_APT_BUILD=false

# Install a local Python
if [ ! -d ${PYTHON_PATH} ]; then
    rm -rf "${PYTHON_PATH_BASEDIR}/python-3*"

    mkdir -p ${PYTHON_PATH_BASEDIR}
    cd ${PYTHON_PATH_BASEDIR}

    curl -o Python-${PYTHON_VERSION}.tar.xz ${PYTHON_URL}
    tar xf Python-${PYTHON_VERSION}.tar.xz
    mv Python-${PYTHON_VERSION} python-${PYTHON_VERSION}
    rm Python-${PYTHON_VERSION}.tar.xz
    cd ${PYTHON_PATH}

    ./configure --prefix=${PYTHON_PATH}
    if [ $? != 0 ]; then
        echo "ERROR: Python ./configure --prefix=${PYTHON_PATH}"
        exit 1
    fi

    make
    if [ $? != 0 ]; then
        echo "ERROR: Python make"
        exit 1
    fi

    make install
    if [ $? != 0 ]; then
        echo "ERROR: Python make install"
        exit 1
    fi

    NEW_PYTHON_BUILD=true
fi

# Setup Python virtual environement
if [ "${NEW_PYTHON_BUILD}" = true ] || [ ! -d ${ANSIBLE_VENV} ]; then
    rm -rf ${ANSIBLE_VENV}

    ${PYTHON_PATH}/bin/python3 -m venv ${ANSIBLE_VENV}
fi

# Install required libs inside Python venv
(
    source ${ANSIBLE_VENV}/bin/activate

    pip3 install --upgrade pip
    pip3 install --upgrade passlib bcrypt

    deactivate
)

# Install python-apt
#
# Since Ansible will be running from a Python venv, there is no default way
# to talk to APT. The `python-apt` lib is the glue between the venv and the
# global system (for APT).
if [ ! -d ${PYTHON_APT_PATH} -o "$NEW_PYTHON_BUILD" = "true" ]; then
    rm -rf "${PYTHON_PATH_BASEDIR}/python-apt*"

    git clone ${PYTHON_APT_URL} ${PYTHON_APT_PATH}
    cd ${PYTHON_APT_PATH}

    # Check versions via `git tag`.
    git checkout ${PYTHON_APT_VERSION}

    # Compile using Python venv.
    (
        source ${ANSIBLE_VENV}/bin/activate

        python3 setup.py build
        python3 setup.py install

        deactivate
    )

    NEW_PYTHON_APT_BUILD=true
fi

# Install Ansible
if [ ! -d ${ANSIBLE_PATH} ]; then
    mkdir -p ${ANSIBLE_BASEDIR}
    git clone ${ANSIBLE_URL} --recursive ${ANSIBLE_PATH}
    cd ${ANSIBLE_PATH}
    git checkout ${ANSIBLE_VERSION_FULL}
else
    cd ${ANSIBLE_PATH}
    git checkout ${ANSIBLE_VERSION_FULL}
    git pull
fi

cp ${HOME_LAN_PATH}/ansible/roles/ansible/files/env-setup ${ANSIBLE_PATH}/hacking/env-setup

(
    source ${ANSIBLE_VENV}/bin/activate

    source ./hacking/env-setup
    pip3 install -r ./requirements.txt

    deactivate
)

mkdir -p ~/bin
echo "export ANSIBLE_HOME=\"${ANSIBLE_PATH}\"" > ~/bin/ansible.in
echo "export PATH=\"\$PATH:\$ANSIBLE_HOME/bin\"" >> ~/bin/ansible.in
echo "export PYTHONPATH=\"\$PYTHONPATH:\$ANSIBLE_HOME/lib\"" >> ~/bin/ansible.in
echo "export MANPATH=\"\$MANPATH:\$ANSIBLE_HOME/docs/man\"" >> ~/bin/ansible.in

for EXEC_NAME in ansible-config ansible-connection ansible-console ansible-doc ansible-galaxy ansible-inventory ansible-playbook ansible-pull ansible-test ansible-vault ansible; do
    echo "#!/bin/bash" > ~/bin/${EXEC_NAME}.sh
    echo "source \"${HOME}/ansible/bin/activate\"" >> ~/bin/${EXEC_NAME}.sh
    echo "source \"${HOME}/bin/ansible.in\"" >> ~/bin/${EXEC_NAME}.sh
    echo "\${ANSIBLE_HOME}/bin/${EXEC_NAME} \$@" >> ~/bin/${EXEC_NAME}.sh
    echo "deactivate" >> ~/bin/${EXEC_NAME}.sh
done
chmod 700 ~/bin/*
